const path = require('path')

module.exports = {
    mode: 'production',
    entry: './src/index.js',
    externals:'lodash',//打包的过程中忽略lodash
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'library.js',//打包文件的 名字
        library: 'root',//root任何一个变量都可以引入  可以使用<script src=''>标签引入的方式
        libraryTarget: 'umd' //值也可以是 this、window、global  //node.js下可以使用global  //任何方式引入 js
    }
}